//****************    POTRZEBNE ZMIENNE I DODATKI    ****************
//podpięcie bazy danych
var Datastore = require('nedb')
//potrzebne rzeczy do działania stron html
var http = require("http")
var express = require("express")
var app = express()
const PORT = 3000;
var path = require("path");
var hbs = require('express-handlebars');
var passwordHash = require('password-hash');
app.use(express.static('static'))
app.set('views', path.join(__dirname, 'views')); // ustalamy katalog views
app.engine('hbs', hbs({ 
    defaultLayout: 'main.hbs' , 
    helpers: {         
        time: function (length) {
            var godziny = Math.floor(length/60);
            var minuty = length - (godziny*60)
            return godziny + " godzin i " + minuty + " minut"
        },
        shortDescription: function (title) {
            return title.substring(0,35) +"...";
        }
    }
}));
app.set('view engine', 'hbs'); // określenie silnika szablonów
var bodyParser = require("body-parser")
app.use(bodyParser.urlencoded({ extended: true })); 
var formidable = require('formidable');
var cookieParser = require("cookie-parser")
app.use(cookieParser()) 
//****************    PODPINANIE BAZ DANYCH    ****************
var info = new Datastore({
    filename: 'admins.db',
    autoload: true
});
var filmy = new Datastore({
    filename: 'filmy.db',
    autoload: true
});
var koszyki = new Datastore({
    filename: 'koszyki.db',
    autoload: true
});
//****************    DZIAŁANIE NA BAZIE DANYCH    ****************
var collection
filmy.find({ }, function (err, docs) {
    //wrzucamy dane z bazy danych do obiektu z nadrzędną nazwą films
    collection= {films: docs}
});
var adminList
info.find({type: "admin"}, function (err, docs) {
    //wrzucamy dane z bazy danych do obiektu z nadrzędną nazwą admin
    adminList= {admin: docs}
});
var LastIndex
info.find({type: "index"}, function (err, docs) {
    //wrzucamy dane z bazy danych do obiektu z nadrzędną nazwą index
    LastIndex= {index: docs}
});
filmy.count({}, function (err, count) {
    collection.hyperlinks = []
    for(var i=1; i<=Math.ceil(count/6); i++)
        collection.hyperlinks[i-1] = i
});
//****************    PO WEJŚCIU NA STRONĘ    ****************
// ***** STRONA GŁÓWNA ***** 
app.get("/", function (req, res) {
    //ustawianie cookies na domyślne
    res.cookie("sortType", "stars", { expires: false})
    res.cookie("sortMethod", NaN, { expires: false})
    res.cookie("page", 0)
    filmy.find({}).limit(6).exec(function (err, docs) {    
        var wybrane= {films: docs}
        wybrane.hyperlinks = collection.hyperlinks
        res.render('show.hbs', wybrane)
    });
})
app.get("/changePage", function (req, res) {
    res.cookie("page",req.query.pageNumber)
    var sortMethod =  parseInt(req.cookies.sortMethod)
    if(req.cookies.sortMethod == NaN){      //nie ma sortowania
        filmy.find({}).limit(6).skip(6*req.query.pageNumber).exec(function (err, docs) {    
            var wybrane= {films: docs}
            wybrane.hyperlinks = collection.hyperlinks
            res.render('show.hbs', wybrane)
        });
    }else if(req.cookies.sortType == "stars"){      //jest sortowanie oceną
        filmy.find({}).sort({ stars : sortMethod }).limit(6).skip(6*req.query.pageNumber).exec(function (err, docs) {    
            var wybrane= {films: docs}
            wybrane.hyperlinks = collection.hyperlinks
            res.render('show.hbs', wybrane)
        });
    }else{                                          //jest sortowanie długością
        filmy.find({}).sort({ title : sortMethod }).limit(6).skip(6*req.query.pageNumber).exec(function (err, docs) {    
            var wybrane= {films: docs}
            wybrane.hyperlinks = collection.hyperlinks
            res.render('show.hbs', wybrane)
        });
    }
})
// ***** PODSTRONY *****
app.get("/film/:id", function(req,res){
    //wyszukujemy element o podanym indexie
    filmy.findOne({ index: req.params.id }, function (err, doc) {
        if(doc != null){
            koszyki.count({ title: doc.title }, function (err, count) {
                 var SelectedItem = {film: doc}
                 SelectedItem.count = count
                 SelectedItem.layout = "items.hbs"
                 res.render('showElement.hbs',SelectedItem) 
             });
        }else{
            res.send("Nie ma takiego elementu w bazie danych")  
        }
    });
})
// ***** SORTOWANIE *****
app.get("/sort", function (req, res) {
    res.cookie("sortType", req.query.sortType, { expires: false})
    res.cookie("sortMethod", parseInt(req.query.sortMethod) , { expires: false})
    substring = new RegExp(req.cookies.substring, "i")
    var sortMethod = parseInt(req.query.sortMethod)
    if(req.query.sortType=="stars"){
        filmy.find({ title: substring }).sort({ stars : sortMethod }).limit(6).exec(function (err, docs) {    
            var wybrane= {films: docs}
            filmy.count({ title: substring }, function (err, count) {
                wybrane.hyperlinks = []
                for(var i=1; i<=Math.ceil(count/6); i++)
                wybrane.hyperlinks[i-1] = i
                res.render('show.hbs', wybrane)
            });
        });
    }else{
        filmy.find({ title: substring }).sort({ title : sortMethod }).limit(6).exec(function (err, docs) {    
            var wybrane= {films: docs}
            filmy.count({ title: substring }, function (err, count) {
                wybrane.hyperlinks = []
                for(var i=1; i<=Math.ceil(count/6); i++)
                wybrane.hyperlinks[i-1] = i
                res.render('show.hbs', wybrane)
            });
        });
    }
})
app.get("/find", function (req, res) {
    res.cookie("substring", req.query.title)
    substring = new RegExp(req.query.title, "i")
    var sortMethod = parseInt(req.query.sortMethod)
    if(req.query.sortType=="stars"){
        filmy.find({ title: substring }).sort({ stars : sortMethod }).limit(6).exec(function (err, docs) {    
            var wybrane= {films: docs}
            filmy.count({ title: substring }, function (err, count) {
                wybrane.hyperlinks = []
                for(var i=1; i<=Math.ceil(count/6); i++)
                wybrane.hyperlinks[i-1] = i
                res.render('show.hbs', wybrane)
            });
        });
    }else{
        filmy.find({ title: substring }).sort({ title : sortMethod }).limit(6).exec(function (err, docs) {    
            var wybrane= {films: docs}
            filmy.count({ title: substring }, function (err, count) {
                wybrane.hyperlinks = []
                for(var i=1; i<=Math.ceil(count/6); i++)
                wybrane.hyperlinks[i-1] = i
                res.render('show.hbs', wybrane)
            });
        });
    }
})
// ****************    DODAWANIE DO KOSZYKA    ****************
//sprawdzanie czy nie ma powtórzeń w zapisywanej tablicy
function powtorzenia (tablica, element){
    for(var i=0; i<tablica.length; i++){
        if(element.title == tablica[i].title)
            return false
    }
    return true
}
//guzik dodania elementu do tymczasowej tablicy na serwerze
app.get("/addBag", function (req, res) {
    if(req.cookies.products != undefined)
        var product = req.cookies.products+";"+req.query.img_url+"_"+req.query.title+"_"+req.query.stars+"_"+req.query.length
    else
        var product = req.query.img_url+"_"+req.query.title+"_"+req.query.stars+"_"+req.query.length  
    res.cookie("products", product , { expires: false})
    var sortMethod =  parseInt(req.cookies.sortMethod)
    if(req.cookies.sortMethod == NaN){      //nie ma sortowania
        filmy.find({}).limit(6).skip(6*req.query.pageNumber).exec(function (err, docs) {    
            var wybrane= {films: docs}
            wybrane.hyperlinks = collection.hyperlinks
            res.render('show.hbs', wybrane)
        });
    }else if(req.cookies.sortType == "stars"){      //jest sortowanie oceną
        filmy.find({}).sort({ stars : sortMethod }).limit(6).skip(6*req.cookies.page).exec(function (err, docs) {    
            var wybrane= {films: docs}
            wybrane.hyperlinks = collection.hyperlinks
            res.render('show.hbs', wybrane)
        });
    }else{                                          //jest sortowanie długością
        filmy.find({}).sort({ title : sortMethod }).limit(6).skip(6*req.cookies.page).exec(function (err, docs) {    
            var wybrane= {films: docs}
            wybrane.hyperlinks = collection.hyperlinks
            res.render('show.hbs', wybrane)
        });
    }    
})
//sprawdzenie tymczasowych elementów zapisyanch w tablicy
app.get("/showBag", function (req, res) {
    var doObejrzenia = []
    if(req.cookies.products != undefined){
        for(var i=0; i<req.cookies.products.split(";").length; i++){
            var film={}
            film.title = req.cookies.products.split(";")[i].split("_")[1]
            film.stars = req.cookies.products.split(";")[i].split("_")[2]
            film.img_url = req.cookies.products.split(";")[i].split("_")[0]
            film.length = req.cookies.products.split(";")[i].split("_")[3]
            if (powtorzenia(doObejrzenia,film))
                doObejrzenia.push(film)
        }
    }
    var koszyk = {films: doObejrzenia}
    res.render('showBag.hbs', koszyk);
})
//usunięcie zapisanej tablicy i przejście do strony głównej
app.get("/removeBag", function (req, res) {
    res.clearCookie("products");
    res.render('show.hbs', collection);
})
//zapisanie elementów tablicy do bazy
app.get("/saveBag", function (req, res) {
    var doObejrzenia = []
    //przygotowanie tablicy
    if(req.cookies.products != undefined){
        for(var i=0; i<req.cookies.products.split(";").length; i++){
            var film={}
            film.title = req.cookies.products.split(";")[i].split("_")[1]
            film.stars = req.cookies.products.split(";")[i].split("_")[2]
            film.img_url = req.cookies.products.split(";")[i].split("_")[0]
            film.length = req.cookies.products.split(";")[i].split("_")[3]
            if (powtorzenia(doObejrzenia,film))
                doObejrzenia.push(film)
        }
    }
    //przejście po tablicy zapisując elementy w bazie danych
    for(var i=0; i<doObejrzenia.length; i++){
        doObejrzenia[i].nick = req.query.nick
        koszyki.insert(doObejrzenia[i], function (err, newDoc) {});
    }
    res.render('show.hbs', collection);
    doObejrzenia = []
    res.clearCookie("products");
})
//wyświetlanie elementów z bazy danych po podaniu nicku
app.get("/showSavedBag", function (req, res) {
    koszyki.find({ nick: req.query.nick }, function (err, docs) {
        var savedElements = {films: docs}
        if(docs.length == 0)
            savedElements.pasek = "Nie ma takiej listy"
        else
            savedElements.pasek = ""
        savedElements.nick = req.query.nick    
        res.render('showSavedElements.hbs', savedElements);
    });  
})
//usunięcie elementów z bazy danych
app.get("/removeSaved", function (req, res) {
    var id = req.query.id
    //usunięcie filmu z wybranym id
    koszyki.remove({ "_id": id }, {}, function (err, numRemoved) {});
    koszyki.find({ nick: req.query.nick }, function (err, docs) {
        var savedElements = {films: docs}    
        res.render('showSavedElements.hbs', savedElements);
    });  
})
//usunięcie całej listy
app.get("/removeSavedList", function (req, res) {
    var nick = req.query.nick
    //usunięcie wszystkich filmów wybranego użytkownika
    koszyki.remove({ "nick": nick }, { multi: true }, function (err, numRemoved) {});
    filmy.find({}).limit(6).exec(function (err, docs) {    
        var wybrane= {films: docs}
        wybrane.hyperlinks = collection.hyperlinks
        res.render('show.hbs', wybrane)
    });
    
})
// ****************    OBSŁUGA ADMINA    ****************
// ***** LOGOWANIE ***** 
/*
action: Wyświetlanie formularza logowania do admina
*/
app.get("/login", function (req, res) {
    if(req.cookies.adminlog){
        res.render('admin.hbs',collection)
    }else{
        adminList.text = "Wejdź do panelu admina!"
        res.render('LoginForm.hbs', adminList);
    }

})
/*
action: Sprawdza czy dane podane w formularzu są prawidłowe jeśli tak to uruchamia panel konfiguracyjny administratora
*/
app.post("/PanelAdmin", function(req, res){
    //sprawdzanie czy podane wartości w formularzu są zgodne z danymi logowania administratora
    if( passwordHash.verify(req.body.login, adminList.admin[0].login) && passwordHash.verify(req.body.password, adminList.admin[0].password) ){
        res.cookie("adminlog", "true", { expires: false , httpOnly:true})
        res.render('admin.hbs',collection)
    }else{
        adminList.text = "Podałeś błędne hasło!"
        res.render('LoginForm.hbs', adminList);
    }
})
// ***** WYLOGOWANIE *****
app.get("/logout", function (req, res) {
    res.clearCookie("adminlog");
    res.render('show.hbs', collection)
})
// ***** USUWANIE ***** 
/*
action: Funkcja usuwania elementów z bazy przez administratora
*/
app.get("/remove", function (req, res) {
    var id = req.query.id
    //usunięcie filmu z wybranym id
    filmy.remove({ "_id": id }, {}, function (err, numRemoved) {
        //stworzenie nowej tabelki z zaktualizowanymi wartościami
        filmy.find({}, function(err,docs){
            collection= {films: docs}
            res.render('admin.hbs',collection);
            //aktualizowanie liczby odnośników
            filmy.count({}, function (err, count) {
                collection.hyperlinks = []
                for(var i=1; i<=Math.ceil(count/6); i++)
                    collection.hyperlinks[i-1] = i
            });
        })
    });
})
// ***** MODYFIKOWANIE ***** 
/*
action: Funkcja włączająca modyfikację wybranego obiektu
*/
app.get("/modify", function (req, res) {
    var edytowany
    filmy.findOne({ _id: req.query.id }, function (err, doc) {
        edytowany = doc
        res.render('modify.hbs',edytowany);
    });
})
/*
action: modyfikowanie elementu wybranego przez administratora, wrzucanie zdjęcia na stronę
*/
app.post('/handleUpload', function (req, res) {
        //wyświetlanie wartości formularza
        var form = new formidable.IncomingForm();
        form.parse(req, function (err, fields, files) {
            //usunięcie starego elementu z kolekcji
            filmy.remove({ _id: fields.id }, {}, function (err, numRemoved) { });
            //stworzenie nowego elementu
            var film = {}
            film.index = fields.index
            film.title = fields.title
            film.stars = fields.stars
            film.img_url = files.imagetoupload.path.slice(files.imagetoupload.path.indexOf("upload"),files.imagetoupload.path.length)
            film.length = fields.length
            film.description = fields.description
            film._id = fields.id
            //wstawienie do bazy nowego elementu
            filmy.insert(film, function (err, newDoc) {});
            //aktualizujemy baze danych
            filmy.find({}, function(err,docs){
                collection= {films: docs}
                res.render('admin.hbs',collection);
            })
        })
        //zapisanie elementu
        form.on('fileBegin', function (name, file){
            file.path = __dirname + '/static/upload/' + file.name;
        });
});
/*
action: Tworzenie nowego elementu na strone 
*/
// ***** DODAWANIE ***** 
app.get("/AddElement", function (req, res) {
    res.render('AddElement.hbs', LastIndex)
})
/*
action: dodanie nowego elementu na strone
*/
app.post("/add", function (req, res) {
    //wyświetlanie wartości formularza
    var form = new formidable.IncomingForm();
    form.parse(req, function (err, fields, files) {
        //stworzenie nowego elementu
        var film = {}
        film.index = LastIndex.index[0].LastIndex.toString()
        film.title = fields.title[0].toUpperCase() + fields.title.substring(1)
        film.stars = fields.stars
        film.img_url = files.imagetoupload.path.slice(files.imagetoupload.path.indexOf("upload"),files.imagetoupload.path.length)
        film.length = fields.length
        film.description = fields.description
        //wstawienie do bazy nowego elementu
        filmy.insert(film, function (err, newDoc) {});
        //modyfikowanie indexu dla nowego elementu
        info.remove({ type:"index" }, {}, function (err, numRemoved) {});
        var index= {}
        index.type = "index"
        var number = parseInt(LastIndex.index[0].LastIndex)+1
        index.LastIndex = number.toString()
        info.insert(index, function (err, newDoc) {});
        //aktualizujemy bazę danych indexów
        info.find({type: "index"}, function (err, docs) {
            //wrzucamy dane z bazy danych do obiektu z nadrzędną nazwą index
            LastIndex= {index: docs}
            //aktualizujemy baze danych
            filmy.find({}, function(err,docs){
                collection= {films: docs}
                //powrót na serwer
                res.render('admin.hbs',collection);
                //aktualizowanie bazy odnośników
                filmy.count({}, function (err, count) {
                    collection.hyperlinks = []
                    for(var i=1; i<=Math.ceil(count/6); i++)
                        collection.hyperlinks[i-1] = i
                });
            })
        });
    })
    //zapisanie elementu
    form.on('fileBegin', function (name, file){
        file.path = __dirname + '/static/upload/' + file.name;
    });
})

// KONIEC SERWERA ----------------------------------------------------------------------------------------------------------------------
//nasłuch na określonym porcie
app.listen(PORT, function () { 
    console.log("start serwera na porcie " + PORT )
})


